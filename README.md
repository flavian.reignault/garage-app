# Projet Web : Garage OnLine

Ce projet consiste au développement d'une application web de type Single Page.  
Pour le sujet, j'ai choisi de réaliser un garage virtuel.  
Dans ce dernier, nous pouvons donc ajouter un véhicule à partir de sa marque et de son modèle.  
Nous pouvons voir la liste de tous nos véhicules en bas à gauche de la page.  
Pour chaque véhicule, il est possible de voir ses détails et de les modifier.  
Il est également possible de supprimer un véhicule du garage.  
Vous trouverez une barre de navigation en haut de la page qui vous permettra de visionner vos véhicule triés en fonction de leur catégorie.  
  
### Lancement de l'application  
Pour lancer l'application, il vous faudra déjà lancer le serveur. Pour cela, allez dans le dossier 'garage-app' et faites la commande suivante :
> ./start-server 
  
Ensuite, il vous suffit d'aller dans le dossier 'app-code' et de lancer le fichier 'index.html' sur un navigateur web.

###### Et voilà, vous êtes prêt à utiliser l'application !

----------
Projet entièrement réalisé par __Flavian REIGNAULT__
