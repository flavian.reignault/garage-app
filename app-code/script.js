$(document).ready(function() {

	// Objet Car en JS
    function Car(brand="<À définir>", model="<À définir>", category="<À définir>", vmax="<À définir>", Oto100="<À définir>", weight="<À définir>", img="<À définir>", id) {
        this.brand = brand;
		this.model = model;
        this.category = category;
        this.vmax = vmax;
		this.Oto100 = Oto100;
		this.weight = weight;
		this.img = img;
        this.id = id;
    }


	// Variables pour la pagination
	let stockGarage = null;
	let firstItem = 0;
	let lastItem = 10;
	const numberOfItems = 10;
	let actualPage = 1;
	let maxPage = 1;



	// Fonctions pour pagination
	function loadCarList() {
		$("#car-list").empty();
		$("#right-col").empty();
		$.get('http://localhost:3000/garage', function(garage) {
			for(let i=firstItem; i < lastItem; i++) {
				if(i < garage.length) {
					fillCarRow(garage[i]);
				}
			};
			stockGarage = garage;
			maxPage = Math.max(Math.ceil(stockGarage.length / numberOfItems), 1);
			showPageInfo();
		});
	}



	// Fonction de tri par catégorie
	function sortByCategory(categorie) {
		$("#car-list").empty();
		$("#right-col").empty();
		document.getElementById('pagination').style.visibility = "hidden";
		let i = 0;
		garage = stockGarage;
		while(i < garage.length) {
			if(garage[i].category == categorie) {
				fillCarRow(garage[i]);
			}
			i++;
		}
	}



	// Fonction refresh
	function refresh() {
		requete = "http://localhost:3000/garage";
        fetch(requete)
        .then(response => response.json())
        .then(function(garage) {
			$("#right-col").empty();
			$("#car-list").empty();
			document.getElementById('pagination').style.visibility = "visible";
			document.getElementById('car-list-title').textContent = 'Mon garage';
			for(let i=firstItem; i < lastItem; i++) {
				if(i < garage.length) {
					fillCarRow(garage[i]);
				}
			};
			stockGarage = garage;
			maxPage = Math.max(Math.ceil(stockGarage.length / numberOfItems), 1);
			showPageInfo();
		})
        .catch(function(error) {
			console.log(error);
		});
	}



	function fillCarRow(car) {
		$('#car-list').append(
			`
			<li class="list-group-item" style="display: flex">
				<span class="lead">
					${car.brand} ${car.model}
				</span>
				<div class="pull-right">
					<form style="display: inline" method="POST" action="/garage/${car.id}" class="details-button-form">
						<button type="submit" class="btn btn-sm btn-success details-button">Détails</button>
					</form>
					<form style="display: inline" method="POST" action="/garage/${car.id}" class="edit-button-form">
						<button type="submit" class="btn btn-sm btn-secondary edit-button">Modifier</button>
					</form>
					<form style="display: inline" method="POST" action="/garage/${car.id}" class="delete-button-form">
						<button type="submit" class="btn btn-sm btn-danger">Supprimer</button>
					</form>
				</div>
			</li>
			`
		);
	}
		
	

	// Récupérer tous les véhicule
	$.get('http://localhost:3000/garage', function() {
		refresh();
	});

	$('nav').on('click', '#navbar-title', function() {
		refresh();
	});

	// Page actuelle / Nombre total de pages
	function showPageInfo() {
		document.getElementById('page-info').innerHTML = `Page ${actualPage} / ${maxPage}`;
	}



	// Aller sur la première page
	$('#pagination').on('click', '.first-page', function() {
		firstItem = 0;
		lastItem = numberOfItems;
		actualPage = 1;
		loadCarList();
	});

	// Aller sur la page précédente
	$('#pagination').on('click', '.previous-page', function() {
		if(firstItem - numberOfItems >= 0) {
			firstItem -= numberOfItems;
			lastItem -= numberOfItems;
			actualPage--;
			loadCarList();
		}
	});

	// Aller sur la page suivante
	$('#pagination').on('click', '.next-page', function() {
		if(firstItem + numberOfItems < stockGarage.length) {
			firstItem += numberOfItems;
			lastItem += numberOfItems;
			actualPage++;
			loadCarList();
		}
	});

	// Aller sur la dernière page
	$('#pagination').on('click', '.last-page', function() {
		firstItem = (maxPage * numberOfItems) - numberOfItems;
		lastItem = (maxPage * numberOfItems);
		actualPage = maxPage;
		loadCarList();
	});



	// Récupérer toutes les hyper cars
	$('.hyper-cars').click(function() {
		sortByCategory('Hyper car');
		document.getElementById('car-list-title').textContent = 'Mon garage : Hyper cars';
	});

	// Récupérer toutes les sport cars
	$('.sport-cars').click(function() {
		sortByCategory('Sport');
		document.getElementById('car-list-title').textContent = 'Mon garage : Sport cars';
	});

	// Récupérer toutes les muscle cars
	$('.muscle-cars').click(function() {
		sortByCategory('Muscle car');
		document.getElementById('car-list-title').textContent = 'Mon garage : Muscle cars';
	});

	// Récupérer toutes les retro cars
	$('.retro-cars').click(function() {
		sortByCategory('Retro');
		document.getElementById('car-list-title').textContent = 'Mon garage : Retro cars';
	});

	// Récupérer toutes les formules 1
	$('.formule-1').click(function() {
		sortByCategory('Formule 1');
		document.getElementById('car-list-title').textContent = 'Mon garage : Formules 1';
	});

	// Récupérer tous les autres véhicules
	$('.autres').click(function() {
		sortByCategory('Autre');
		document.getElementById('car-list-title').textContent = 'Mon garage : Autres véhicules';
	});

	

	// Ajouter un véhicule
	$('#new-car-form').submit(function(e) {
		e.preventDefault();
		var newCar = new Car(
			$('#brand-new-car').val(),
			$('#model-new-car').val()
		);
		let actionUrl = 'http://localhost:3000' + $(this).attr('action');
		$.ajax({
			url: actionUrl,
			type: 'POST',
			data: newCar,
			success: function(car) {
				refresh();
				fillCarRow(car);
			}
		});

		$('#new-car-form').get(0).reset();
		$('#new-car-form').get(1).reset();
		$(this).find('button').blur();
	});



	// Ouvrir les détails d'un véhicule
	$('#car-list').on('submit', '.details-button-form', function(e) {
		e.preventDefault();
		let actionUrl = 'http://localhost:3000' + $(this).attr('action');
		fetch(actionUrl)
		.then(response => response.json())
		.then(function(car) {
			$("#right-col").empty();
			$('#right-col').append(
				`
				<div id="current-car">
					<h1 style="margin-bottom: 30px">${car.brand} ${car.model}</h1>
					<img class="car-img" src="${car.img}">
					<table style="font-size: 18px" class="table table-bordered">
						<tr>
							<td class="details-title">Catégorie</td>
							<td class="details-content">${car.category}</td>
						</tr>
						<tr>
							<td class="details-title">Vitesse max</td>
							<td class="details-content">${car.vmax} km/h</td>
						</tr>
						<tr>
							<td class="details-title">0 à 100 km/h</td>
							<td class="details-content">${car.Oto100} secondes</td>
						</tr>
						<tr>
							<td class="details-title">Poids du véhicule</td>
							<td class="details-content">${car.weight} kg</td>
						</tr>
					</table>
				</div>
				`
			);
		})
	});


	let stockCar = null;


	// Ouvrir le formulaire des détails d'un véhicule
	$('#car-list').on('submit', '.edit-button-form', function(e) {
		e.preventDefault();
		let actionUrl = 'http://localhost:3000' + $(this).attr('action');
		stockCar = $(this).closest('.list-group-item');
		fetch(actionUrl)
        .then(response => response.json())
        .then(function(car) {
			$("#right-col").empty();
			$('#right-col').append(
				`
				<h1 style="margin-bottom: 30px">${car.brand} ${car.model}</h1>
				<form action="/garage/${car.id}" method="POST" id="current-car-form">
					<div class="form-group">
						<table style="font-size: 18px" class="w-100">
							<tr>
								<td><label for="${car.id}" style="font-weight: bold">Marque du véhicule</label></td>
								<td><label for="${car.id}" style="font-weight: bold">Model du véhicule</label></td>
							</tr>
							<tr>
								<td><input type="text" style="margin-bottom: 20px" value="${car.brand}" name="brand" class="input-brand form-control" id="${car.id}"></td>
								<td><input type="text" style="margin-bottom: 20px" value="${car.model}" name="model" class="input-model form-control" id="${car.id}"></td>
							</tr>
						</table>
						<label for="${car.id}">Catégorie du véhicule</label><br>
						<select name="category" class="input-category form-select" id="${car.id}">
							<option value="">>----- Choisissez une proposition -----<</option>
							<option value="Hyper car">Hyper car</option>
							<option value="Sport">Sport</option>
							<option value="Muscle car">Muscle car</option>
							<option value="Retro">Retro</option>
							<option value="Formule 1">Formule 1</option>
							<option value="Autre">Autre</option>
						</select><br>
						<label for="${car.id}">Vitesse max (en km/h)</label>
						<input type="text" value="${car.vmax}" name="type" class="input-vmax form-control" id="${car.id}">
						<label for="${car.id}">0 à 100 km/h (en s)</label>
						<input type="text" value="${car.Oto100}" name="type" class="input-0to100 form-control" id="${car.id}">
						<label for="${car.id}">Poids du véhicule (en kg)</label>
						<input type="text" value="${car.weight}" name="type" class="input-weight form-control" id="${car.id}">
						<label for="${car.id}">Photo du véhicule</label><br>
						<input type="file" value="${car.img}" class="input-img form-control" accept="image/jpg, image/jpeg, image/png" id="${car.id}">
					</div>
					<button style="margin-top: 10px" type="submit" id="update-button" class="btn btn-primary">Valider les modifications</button>
				</form>
				`
			);
		})
        .catch(function(error) {
			console.log(error);
		});
	});


	// Éditer les informations d'un véhicule
	$('#right-col').on('submit', '#current-car-form', function(e) {
		e.preventDefault();
		let newCar = new Car(
			$('.input-brand').val(),
			$('.input-model').val(),
			$('.input-category').val(),
			$('.input-vmax').val(),
			$('.input-0to100').val(),
			$('.input-weight').val(),
			'img\\' + $('.input-img').val().toString().slice(12,1000)
		);
		let actionUrl = 'http://localhost:3000' + $(this).attr('action');
		let $originalCar = stockCar;
		$.ajax({
			url: actionUrl,
			type: 'PUT',
			data: newCar,
			originalCar: $originalCar,
			success: function(car) {
				stockCar.empty();
				stockCar.append(
					`
					<span class="lead">
						${car.brand} ${car.model}
					</span>
					<div class="pull-right">
						<form style="display: inline" method="POST" action="/garage/${car.id}" class="details-button-form">
							<button type="submit" class="btn btn-sm btn-success details-button">Détails</button>
						</form>
						<form style="display: inline" method="POST" action="/garage/${car.id}" class="edit-button-form">
							<button type="submit" class="btn btn-sm btn-secondary edit-button">Modifier</button>
						</form>
						<form style="display: inline" method="POST" action="/garage/${car.id}" class="delete-button-form">
							<button type="submit" class="btn btn-sm btn-danger">Supprimer</button>
						</form>
					</div>
					`
				);
				refresh();
			}
		});
	});



	// Supprimer une voiture
	$('#car-list').on('submit', '.delete-button-form', function(e) {
		e.preventDefault();
		let reponse = confirm('Êtes-vous sûr de vouloir retirer ce véhicule de votre garage ?');
		if(reponse) {
			let actionUrl = 'http://localhost:3000' + $(this).attr('action');
			fetch(actionUrl, {method: 'DELETE'})
			.then(response => response.json())
			.then(function() {
				refresh();
			})
		} 
		else {
			$(this).find('button').blur();
		}
	});

});